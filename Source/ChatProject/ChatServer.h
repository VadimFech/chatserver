// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "ChatServer.generated.h"

UCLASS()
class CHATPROJECT_API AChatServer : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AChatServer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void EndPlay(const EEndPlayReason::Type EndPlayReason);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void StartServer();
	void HandleNewConnection();
	void HandleClientDisconnection(FSocket* ClientSocket);
	void HandleMessageFromClient(FSocket* ClientSocket);
	void BroadcastMessage(const TArray<uint8>& Message, FSocket* Sender);
	FString GetNicknameForSocket(FSocket* ClientSocket);

	FSocket* ServerSocket;
	TArray<FSocket*> ClientSockets;
	bool bIsRunning;

	// ����� ��� �������� ��������� ��������
	TMap<FSocket*, FString> ClientNicknames;
	TMap<FString, TArray<FSocket*>> ChatRooms;
	TMap<FSocket*, FString> ClientRooms;

	void HandleCreateRoom(FSocket* ClientSocket, const FString& RoomName);
	void HandleJoinRoom(FSocket* ClientSocket, const FString& RoomName);
	void SendRoomList();
	void SendRoomListToClient(FSocket* ClientSocket);
	void SendUserListToRoom(const FString& RoomName);
};
