// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ChatProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHATPROJECT_API AChatProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
