#include "ChatServer.h"
#include "Engine/Engine.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Networking.h"

// Sets default values
AChatServer::AChatServer()
{
    PrimaryActorTick.bCanEverTick = true;
    ServerSocket = nullptr;
    bIsRunning = false;
}

// Called when the game starts or when spawned
void AChatServer::BeginPlay()
{
    Super::BeginPlay();
    StartServer();
}

void AChatServer::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    if (ServerSocket)
    {
        ServerSocket->Close();
        ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ServerSocket);
    }

    for (FSocket* ClientSocket : ClientSockets)
    {
        if (ClientSocket)
        {
            ClientSocket->Close();
            ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ClientSocket);
        }
    }

    Super::EndPlay(EndPlayReason);
}

// Called every frame
void AChatServer::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (bIsRunning)
    {
        HandleNewConnection();
        for (int32 i = ClientSockets.Num() - 1; i >= 0; --i)
        {
            FSocket* ClientSocket = ClientSockets[i];
            uint32 Size;
            if (ClientSocket && ClientSocket->HasPendingData(Size))
            {
                HandleMessageFromClient(ClientSocket);
            }
        }
    }
}

void AChatServer::StartServer()
{
    ISocketSubsystem* SocketSubsystem = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);
    ServerSocket = SocketSubsystem->CreateSocket(NAME_Stream, TEXT("Chat Server"), false);

    // ������������� ���������� IP-������ ��� �������������
    FIPv4Address IP;
    FIPv4Address::Parse(TEXT("0.0.0.0"), IP); // ������������� �� ���� ��������� IP-�������
    int32 Port = 7878;
    TSharedRef<FInternetAddr> Addr = SocketSubsystem->CreateInternetAddr();
    Addr->SetIp(IP.FIPv4Address::Value);
    Addr->SetPort(Port);

    bool bCouldBind = ServerSocket->Bind(*Addr);
    bool bIsListening = ServerSocket->Listen(8); // �� 8 �������� � �������

    if (bCouldBind && bIsListening)
    {
        bIsRunning = true;
        UE_LOG(LogTemp, Warning, TEXT("Server started on port %d"), Port);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to start server"));
    }
}

void AChatServer::HandleNewConnection()
{
    bool bPending;
    if (ServerSocket && ServerSocket->HasPendingConnection(bPending) && bPending)
    {
        FSocket* ClientSocket = ServerSocket->Accept(TEXT("Incoming connection"));
        if (ClientSocket)
        {
            UE_LOG(LogTemp, Warning, TEXT("New client connected"));
            ClientSockets.Add(ClientSocket);
        }
        else
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to accept new client"));
        }
    }
}

void AChatServer::HandleMessageFromClient(FSocket* ClientSocket)
{
    uint32 Size;
    while (ClientSocket->HasPendingData(Size))
    {
        TArray<uint8> ReceivedData;
        ReceivedData.SetNumUninitialized(FMath::Min(Size, 65507u));

        int32 Read = 0;
        ClientSocket->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);

        FString ReceivedString = FString(UTF8_TO_TCHAR(ReceivedData.GetData()));

        // �������� �� ��������� �� ����������
        if (ReceivedString.StartsWith(TEXT("DISCONNECT:")))
        {
            HandleClientDisconnection(ClientSocket);
            return;
        }

        // �������� �� ��������� � �������� �������
        if (ReceivedString.StartsWith(TEXT("CREATE_ROOM:")))
        {
            FString RoomName = ReceivedString.RightChop(12);
            HandleCreateRoom(ClientSocket, RoomName);
        }
        // �������� �� ��������� � ������������� � �������
        else if (ReceivedString.StartsWith(TEXT("JOIN_ROOM:")))
        {
            FString RoomName = ReceivedString.RightChop(10);
            HandleJoinRoom(ClientSocket, RoomName);
        }
        // �������� �� ��������� � ������� ������ ������
        else if (ReceivedString.StartsWith(TEXT("REQUEST_ROOMLIST")))
        {
            SendRoomListToClient(ClientSocket);
        }
        // �������� �� ��������� � ������� ������ �������� � �������
        else if (ReceivedString.StartsWith(TEXT("REQUEST_USERLIST:")))
        {
            FString RoomName = ReceivedString.RightChop(17);
            SendUserListToRoom(RoomName);
        }
        else if (!ClientNicknames.Contains(ClientSocket))  // ���� ��� ������ ��������� �� �������, ������� ��� ���������
        {
            if (!ReceivedString.IsEmpty())
            {
                ClientNicknames.Add(ClientSocket, ReceivedString);
                UE_LOG(LogTemp, Warning, TEXT("Client nickname set: %s"), *ReceivedString);
            }
            else
            {
                UE_LOG(LogTemp, Error, TEXT("Received empty nickname from client"));
            }
        }
        else
        {
            FString Nickname = ClientNicknames[ClientSocket];
            FString FullMessage = ReceivedString;
            // ����������� ����������� ���������
            UE_LOG(LogTemp, Warning, TEXT("Message from client: %s"), *FullMessage);

            // ��������� ���������, �������� ������ ��������
            TArray<uint8> MessageData;
            FTCHARToUTF8 Convert(*FullMessage);
            MessageData.Append((uint8*)TCHAR_TO_UTF8(*FullMessage), FullMessage.Len() + 1);
            BroadcastMessage(MessageData, ClientSocket);
        }
    }
}

void AChatServer::BroadcastMessage(const TArray<uint8>& Message, FSocket* Sender)
{
    FString SenderRoom = ClientRooms[Sender]; // �������� ������� �����������

    for (FSocket* ClientSocket : ClientSockets)
    {
        if (ClientSocket != Sender && ClientRooms[ClientSocket] == SenderRoom) // ���������, ��� ������ � ��� �� �������
        {
            int32 Sent = 0;
            bool bSuccessful = ClientSocket->Send(Message.GetData(), Message.Num(), Sent);

            if (!bSuccessful || Sent != Message.Num())
            {
                UE_LOG(LogTemp, Error, TEXT("Failed to broadcast message to client"));
            }
        }
    }
}

void AChatServer::SendRoomList()
{
    TArray<FString> RoomNames;
    ChatRooms.GetKeys(RoomNames);

    FString RoomListString = TEXT("ROOMLIST:") + FString::Join(RoomNames, TEXT(","));
    TArray<uint8> RoomListData;
    FTCHARToUTF8 Convert(*RoomListString);
    RoomListData.Append((uint8*)TCHAR_TO_UTF8(*RoomListString), RoomListString.Len());

    for (FSocket* ClientSocket : ClientSockets)
    {
        int32 Sent = 0;
        bool bSuccessful = ClientSocket->Send(RoomListData.GetData(), RoomListData.Num(), Sent);

        if (!bSuccessful || Sent != RoomListData.Num())
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to send room list to client"));
        }
    }
}

FString AChatServer::GetNicknameForSocket(FSocket* ClientSocket)
{
    if (ClientNicknames.Contains(ClientSocket))
    {
        return ClientNicknames[ClientSocket];
    }
    return TEXT("Unknown");
}

void AChatServer::HandleClientDisconnection(FSocket* ClientSocket)
{
    if (ClientNicknames.Contains(ClientSocket))
    {
        FString Nickname = ClientNicknames[ClientSocket];
        FString RoomName = ClientRooms[ClientSocket];
        ClientNicknames.Remove(ClientSocket);
        ClientRooms.Remove(ClientSocket);
        ChatRooms[RoomName].Remove(ClientSocket);
        ClientSockets.Remove(ClientSocket);
        ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ClientSocket);
        UE_LOG(LogTemp, Warning, TEXT("Client disconnected: %s"), *Nickname);
        SendUserListToRoom(RoomName);
    }
}

void AChatServer::HandleCreateRoom(FSocket* ClientSocket, const FString& RoomName)
{
    if (!ChatRooms.Contains(RoomName))
    {
        ChatRooms.Add(RoomName, TArray<FSocket*>());
        UE_LOG(LogTemp, Warning, TEXT("Room created: %s"), *RoomName);
        SendRoomList();
    }
}

void AChatServer::HandleJoinRoom(FSocket* ClientSocket, const FString& RoomName)
{
    if (ChatRooms.Contains(RoomName))
    {
        ChatRooms[RoomName].Add(ClientSocket);
        ClientRooms.Add(ClientSocket, RoomName); // ��������� ������� � ����� ������
        UE_LOG(LogTemp, Warning, TEXT("Client joined room: %s"), *RoomName);
        SendRoomList();
    }
}

void AChatServer::SendRoomListToClient(FSocket* ClientSocket)
{
    TArray<FString> RoomNames;
    ChatRooms.GetKeys(RoomNames);

    FString RoomListString = TEXT("ROOMLIST:") + FString::Join(RoomNames, TEXT(","));
    TArray<uint8> RoomListData;
    FTCHARToUTF8 Convert(*RoomListString);
    RoomListData.Append((uint8*)TCHAR_TO_UTF8(*RoomListString), RoomListString.Len());

    int32 Sent = 0;
    bool bSuccessful = ClientSocket->Send(RoomListData.GetData(), RoomListData.Num(), Sent);

    if (!bSuccessful || Sent != RoomListData.Num())
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to send room list to client"));
    }
}

void AChatServer::SendUserListToRoom(const FString& RoomName)
{
    if (!ChatRooms.Contains(RoomName))
    {
        return;
    }

    TArray<FString> Usernames;
    for (FSocket* ClientSocket : ChatRooms[RoomName])
    {
        FString Nickname = GetNicknameForSocket(ClientSocket);
        Usernames.Add(Nickname);
    }

    FString UserListString = TEXT("USERLIST:") + FString::Join(Usernames, TEXT(","));
    TArray<uint8> UserListData;
    FTCHARToUTF8 Convert(*UserListString);
    UserListData.Append((uint8*)TCHAR_TO_UTF8(*UserListString), UserListString.Len());

    for (FSocket* ClientSocket : ChatRooms[RoomName])
    {
        int32 Sent = 0;
        bool bSuccessful = ClientSocket->Send(UserListData.GetData(), UserListData.Num(), Sent);

        if (!bSuccessful || Sent != UserListData.Num())
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to send user list to client"));
        }
    }
}